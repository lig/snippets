"""One of my own Code Style rules that help me write clean and easy maintainable code
is: the default value of a boolean should always be 'false'.

(from: https://fosstodon.org/@lig/110338236238836648)
"""

def make_me_coffee(milk: bool = True, sugar: bool = False) -> Drink:
    """                         ^^^^
    This is bad and non-obvious! Boolean defaults should be always `False`
    """

def make_me_coffee(no_milk: bool = False, sugar: bool = False) -> Drink:
    """            ^^^^^^^
    This is better but confusing. Use positive booleans if possible.
    """

def make_me_coffee(milk: bool = False, sugar: bool = False) -> Drink:
    """            ^^^^         ^^^^^
    This is the best! But what if I want my coffee with milk by default?
    """

def make_me_coffee_with_milk(sugar: bool = False) -> Drink:
    """
    Just make another function. But I don't ever use sugar...
    """
    return make_me_coffee(milk=True, sugar=sugar)

def make_my_favorite_coffee() -> Drink:
    """
    Your coffee, your rules!
    """
    return make_me_coffee(milk=True)


"""So, what is the reasoning?

There are a number of cases where this helps. Mostly this tends to help with
understanding and extending large code bases. You would like keep things do
things you expect them to do and not rely on your memory or documentation as
both tend to go different ways with the code through time.
"""

def case_keep_things_sane(drive_me_crazy: boolean = True):
    """
    The body of function suggests that `drive_me_crazy` is a special case.
    However, looking at the function signature it is the default.
    """
    if drive_me_crazy:
        print("(⊙_☉)")
    print("It's alright!")

def case_extending_things():
    """
    This is a previous version of the function and we are extending its API in
    the next version. 
    """

def case_extending_things(enable_new_fancy_thing: bool = True):
    """
    Obviously, this default breaks everything used the previous version of the
    function and nobody does that in the real life. I hope.
    """

def case_i_am_smarter_that_if_statement(do_not_mess_with_me: bool = True):
    """This is a slight turn of the `case_keep_things_sane` case.
    
    This particular one often comes from adding dirty hacks without taking
    a little break and thinking about readability of the code. Despite obvious
    readability issues this one could event introduce unwanted behavior in
    Python. Some object could found it's way into such clause that has its
    default value coercing to `bool` as `True`.
    """
    if not do_not_mess_with_me:
        print("You are always messing with me:(")


"""Overall, it's incredibly convenient to assume that if a boolean is `True`
then somewhat somewhere requested it to be that way."""
